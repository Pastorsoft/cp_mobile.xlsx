const fs = require("fs");
const XSLX = require("xlsx");

const exelAjson = () => {
  const excel = XSLX.readFile("rondas.xlsx");
  const nombreHoja = excel.SheetNames;

  let datos = XSLX.utils.sheet_to_json(excel.Sheets[nombreHoja[0]]);

  console.log(datos[0]);
  const jDatos = [];
  for (let index = 0; index < datos.length; index++) {
    jDatos.push({ id: index, ...datos[index] });
  }

  fs.writeFile("nuevo.json", JSON.stringify(jDatos), "utf8", (err) => {
    if (err) throw err;
    console.log(err);
  });
};

exelAjson();
